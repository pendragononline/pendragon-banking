$(document).ready(function () {

    //Hide all open info panels on panel click
    $('.info-panel-slideshow').click(function () {
        $(this).slideUp();
    });

    //Set first slides as active and open panels
    $('#slideshow-button1').css('background-color', '#00B8E9');
    $('#slideshow-info-1').css('display', 'block');

    //Active
    $('.slideshow-button').click(function () {
        $(this).css('background-color', '#00B8E9');
        $(".slideshow-button").not(this).css('background-color', '#008CE9');
    });

    //Slide 1
    $("#slideshow-button1").click(function () {
        $('#dragonbase-slideshow').css("background-image", "url(images/slideshow/3.jpg)");

        //Hide any other open info boxes
        $('.info-panel-slideshow:not(#slideshow-info-1)').slideUp();

        //Show the appropriate info box
        $('#slideshow-info-1').slideToggle();
        $('#slideshow-info-1').css('display', 'block');
    });

    //Slide 2
    $("#slideshow-button2").click(function () {
        $('#dragonbase-slideshow').css("background-image", "url(images/slideshow/2.jpg)");

        //Hide any other open info boxes
        $('.info-panel-slideshow:not(#slideshow-info-2)').slideUp();

        //Show the appropriate info box
        $('#slideshow-info-2').slideToggle();
        $('#slideshow-info-2').css('display', 'block');
    });

    //Slide 3
    $("#slideshow-button3").click(function () {
        $('#dragonbase-slideshow').css("background-image", "url(images/slideshow/1.jpg)");

        //Hide any other open info boxes
        $('.info-panel-slideshow:not(#slideshow-info-3)').slideUp();

        //Show the appropriate info box
        $('#slideshow-info-3').slideToggle();
        $('#slideshow-info-3').css('display', 'block');
    });

    //Slide 4
    $("#slideshow-button4").click(function () {
        $('#dragonbase-slideshow').css("background-image", "url(images/slideshow/4.jpg)");

        //Hide any other open info boxes
        $('.info-panel-slideshow:not(#slideshow-info-4)').slideUp();

        //Show the appropriate info box
        $('#slideshow-info-4').slideToggle();
        $('#slideshow-info-4').css('display', 'block');
    });

    
});



    