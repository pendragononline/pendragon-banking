$(document).ready(function () {

    //Highlight selected div
    $('.menu-button').mouseenter(function () {
        $(this).css('background-color', '#00B8E9');
        $(this).css('color', 'white');
        $(".menu-button").not(this).css('background-color', 'white');
        $(".menu-button").not(this).css('color', 'black');
    });

    //You
    $('#you-button').mouseenter(function () {
        $('.mega-menu:not(#you-mega-menu)').slideUp();
        $('#you-mega-menu').slideToggle();
        $('#you-mega-menu').css('display', 'block');
    });

    //Accounts
    $('#accounts-button').mouseenter(function () {
        $('.mega-menu:not(#accounts-mega-menu)').slideUp();
        $('#accounts-mega-menu').slideToggle();
        $('#accounts-mega-menu').css('display', 'block');
    });

    //Credit Cards
    $('#cards-button').mouseenter(function () {
        $('.mega-menu:not(#cards-mega-menu)').slideUp();
        $('#cards-mega-menu').slideToggle();
        $('#cards-mega-menu').css('display', 'block');
    });

    //Mortgages
    $('#mortgages-button').mouseenter(function () {
        $('.mega-menu:not(#mortgages-mega-menu)').slideUp();
        $('#mortgages-mega-menu').slideToggle();
        $('#mortgages-mega-menu').css('display', 'block');
    });

    //Investments/Savings
    $('#investing-button').mouseenter(function () {
        $('.mega-menu:not(#investing-mega-menu)').slideUp();
        $('#investing-mega-menu').slideToggle();
        $('#investing-mega-menu').css('display', 'block');
    });

    //Insurance
    $('#insurance-button').mouseenter(function () {
        $('.mega-menu:not(#insurance-mega-menu)').slideUp();
        $('#insurance-mega-menu').slideToggle();
        $('#insurance-mega-menu').css('display', 'block');
    });

    //Life Events
    $('#lifeevents-button').mouseenter(function () {
        $('.mega-menu:not(#lifeevents-mega-menu)').slideUp();
        $('#lifeevents-mega-menu').slideToggle();
        $('#lifeevents-mega-menu').css('display', 'block');
    });

    //Guidance 
    $('#guidance-button').mouseenter(function () {
        $('.mega-menu:not(#guidance-mega-menu)').slideUp();
        $('#guidance-mega-menu').slideToggle();
        $('#guidance-mega-menu').css('display', 'block');
    });

});
